import React from "react"
import Button from '@material-ui/core/Button';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import { Link } from "gatsby"
import Layout from "../../components/layout"
import SEO from "../../components/seo"

const SecondPage = () => {
  return (
    <Layout>
      <SEO title="Data from markdown" />
      <Card>
        <CardContent>
          <div>no content</div>
        </CardContent>
        <CardActions>
          <Button variant="contained" color="secondary" size="small"><Link to="/">Go back to the homepage</Link></Button>
        </CardActions>
      </Card>
    </Layout>
  )
}

export default SecondPage
